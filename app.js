/*********************************
**********************************
****                         *****
****  General Configurations *****
****                         *****
**********************************
**********************************/
const routes = require('./config/routes');

/*********************************
**********************************
****                         *****
****       Dependencies      *****
****                         *****
**********************************
**********************************/
const express                 = require('express'),
      app                     = express(),
      bodyParser = require('body-parser'),
      path                    = require('path'),
      expressRouteController  = require('express-route-controller2');

/***************
****  CORS  ****
****************/

var allowCrossDomain = require('./filters/allow_origins');
app.use(allowCrossDomain);

// Body parser for json files
app.use(bodyParser.json());

// Controllers folder
expressRouteController(app, {
    controllers: __dirname + '/controllers',
    routes: routes
});

// Specify static folder
app.use(express.static(path.join(__dirname, 'public')));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  // res.sendStatus(err.status || 500);
  res.json({
    'error':{
      msg: err.message,
      error: {}
    },
      'status': err.status || 500
  });
});

module.exports = app;