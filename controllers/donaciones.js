const async = require('async')
	  bd 	= require('../bd');

module.exports = {

    getDonaciones: function(req,res,next){
        console.log("Entro en getDonacionesByUserId de donaciones.js");
        console.log(req.body);

        bd.getDonacionesByUserId(req.body,function(err, rows){
            if (err) {
                res.send();
                return
            }
            else {
                res.json({
                    rows:rows
                });
            }
        });
    },

    postDonacion: function(req,res,next){
		console.log("Entro en insertDonacion de medicamentos.js");
        console.log(req.body);

        bd.insertDonacion(req.body,function(err, rows){
            if (err) {
                res.send(err);
                return
            }
            res.json({
                rows:rows
            });
        });
    }
}