const async = require('async')
	  bd 	= require('../bd');

module.exports = {

	getMedicamentos: function(req,res,next){
		// var medsArray = [];
	    bd.getMedicamentos(function(err, rows){
            console.log("El json es:")
			console.log(rows);
	    	res.json({
	          medicines:rows
	        });
	    });
	},

	postMedicamentos: function(req,res,next){
        console.log("Entro en insertMedicamento.js");
        console.log(req.body);

        bd.insertMedicamentos(req.body,function(err, rows){
            if (err)
                res.send();

            res.json({
                rows:rows
            });
        });
	}
}