const async = require('async')
	  bd 	= require('../bd');

module.exports = {

    getUsuarios: function(req,res,next){

        bd.getUsuarios(function(err, rows){
            res.json({
              rows:rows
            });
        });
    },

    postUsuarios: function(req, res, next){

        // Recibe usuarios en el formato
        // {
        //     "id_tipo_usuario" : "*",
        //     "email" : "*@*.*",
        //     "contraseña" : "*",
        //     "año_nacimiento" : "aaaa-mm-dd hh:mm:ss",
        //     "nombre" : "*",
        //     "cedula_rif" : "*",
        //     "url_rif_escaneado" : "*",
        //     "telefono" : "*"
        // }

        var user = req.body;

        bd.insertUsuarios(user, function(err, rows){
            res.status(201).json({
                msg: 'Creacion Exitosa',
                rows: rows
            });
        });
    },


    //Funcion para que me indica si el usuario ya existe segun su id
    existeUsuarioId: function(req, res, next){

        var id = req.body.id_usuario;

        if(id){
            bd.getUsuarioById(id, function(err, rows){

                if (rows.length > 0){
                    req.existe = true;
                }else{
                    req.existe = false;
                }
                next();
            });
        }else{
            res.status(400).json({
                  msg: 'Falta campo id de usuario'
                });
        }
    },

    //Funcion para que me indica si el usuario ya existe segun su correo
    existeUsuarioMail: function(req, res, next){

        var email = req.body.email;

        bd.getUsuarioByMail(email, function(err, rows){

            if (rows.length > 0){
                res.status(412).json({

                    msg: 'Usuario ya existe'
                });
            }else{
                next();
            }
        });
    },
    getDonadores: function(req,res,next){
        console.log("Entro en getDonadores de usuarios.js");
        console.log(req.body);
        var centro = req.body;

        bd.getDonadores(centro.centroid, function(err, rows){
            if (err) {
                res.send();
                return
            }
            else {
                res.json({
                    rows:rows
                });
            }
        });
    }
};
