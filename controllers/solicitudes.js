const async = require('async')
	  bd 	= require('../bd');

module.exports = {

    // getDonaciones: function(req,res,next){
    //     console.log("Entro en getDonacionesByUserId de donaciones.js");
    //     console.log(req.body);
    //
    //     bd.getDonacionesByUserId(req.body,function(err, rows){
    //         if (err) {
    //             res.send();
    //             return
    //         }
    //         else {
    //             res.json({
    //                 rows:rows
    //             });
    //         }
    //     });
    // },

    getSolicitudes: function(req,res,next){
        console.log("Entro en getSolicitudes de solicitudes.js");
        console.log(req.body);

        bd.getSolicitudes(req.body,function(err, rows){
            if (err) {
                res.send(err);
                return
            }
            res.json({
                rows:rows
            });
        });
    },


    getReceptores: function(req,res,next){
        console.log("Entro en getReceptores de solicitudes.js");
        console.log(req.body);

        bd.getReceptores(req.body,function(err, rows){
            if (err) {
                res.send(err);
                return
            }
            res.json({
                rows:rows
            });
        });
    },


    postSolicitud: function(req,res,next){
		console.log("Entro en postSolicitud de solicitudes.js");
        console.log(req.body);

        bd.insertSolicitud(req.body,function(err, rows){
            if (err) {
                res.send(err);
                return
            }
            res.json({
                rows:rows
            });
        });
    }
}