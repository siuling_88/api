const async = require('async')
	  bd 	= require('../bd');

module.exports = {

	getMedicos: function(req,res,next){

	    bd.getMedicos(function(err, rows){
	        res.json({
	          rows:rows
	        });
	    });
	},

	postMedicos: function(req,res,next){
		// Recibe medicos en el formato
		// {
		// 	"id_usuario": "*",
		// 	"clave_mpps": "*", //Debe ser un numero entero
		// 	"especialidad": "*"
		// }

		if(req.existe){
			var medico = req.body;

		    bd.insertMedico(medico, function(err, rows){
		        res.status(201).json({
			      msg: 'Creacion Exitosa'
			    });
		    });

		}else{
			res.status(400).json({msg:'El usuario no existe'});
		}
	},

	//Funcion MiddleWare Para chequear si la clavempps es un entero
	claveEsInt: function(req,res,next){

	    var clave = req.body.clave_mpps;

	    if(clave){
	    	if(!isNaN(parseInt(clave, 10))){
	    		next();
	    	}else{
	    		res.status(400).json({msg:'Clave MPPS debe ser un numero entero'});
	    	}
	    }
	},

}