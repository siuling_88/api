# My project's README

This is for you to read me, ok? 
OK...

# Aqui empieza el de verdad

## Recibe: ##
- Content-Type: application/xml
- XML Binario o en String

## Para correr: ##
- Tener node mas nuevo (estoy usando ahorita el v6.3.1)
- Hacer 'npm install' para que baje las dependencias del package.json.
- Correr bin/www

## Para la BD(mysql): ##
- Se esta usando este conector : https://github.com/mysqljs/mysql#escaping-query-values
- Ahi se pueden ver ejemplos de query y como usarlo.