module.exports = {
    
    "/api/login": {
        "post":{
            "middleware":[],
            "handler": "login#checkLogin"
        }
    },

    "/api/usuarios": {
        "get":{
            "middleware":[],
            "handler": "usuarios#getUsuarios"
        },
        "post":{
            "middleware":["usuarios#existeUsuarioMail"],
            "handler": "usuarios#postUsuarios"
        }
    },

    "/api/donadores": {
        "post":{
            "middleware":[],
            "handler": "usuarios#getDonadores"
        }
    },

    "/api/medicos": {
        "get":{
            "middleware":[],
            "handler": "medicos#getMedicos"
        },
        "post":{
            "middleware":["usuarios#existeUsuarioId", "medicos#claveEsInt"],
            "handler": "medicos#postMedicos"
        }
    },

    "/api/medicamentos": {
        "get":{
            "middleware":[],
            "handler": "medicamentos#getMedicamentos"
        },
        "post":{
            "middleware":[],
            "handler": "medicamentos#postMedicamentos"
        }
    },

    "/api/solicitudes": {
        // "get":{
        //     "middleware":[],
        //     "handler": "donaciones#getDonacion"
        // },
        "post":{
            "middleware":[],
            "handler": "solicitudes#postSolicitud"
        }
    },

    "/api/getSolicitudes": {
        // "get":{
        //     "middleware":[],
        //     "handler": "donaciones#getDonacion"
        // },
        "post":{
            "middleware":[],
            "handler": "solicitudes#getSolicitudes"
        }
    },

    "/api/getReceptores": {
        // "get":{
        //     "middleware":[],
        //     "handler": "donaciones#getDonacion"
        // },
        "post":{
            "middleware":[],
            "handler": "solicitudes#getReceptores"
        }
    },

    "/api/donaciones": {
        // "get":{
        //     "middleware":[],
        //     "handler": "donaciones#getDonacion"
        // },
        "post":{
            "middleware":[],
            "handler": "donaciones#postDonacion"
        }
    },

    "/api/donacionesById": {
        "get":{
            "middleware":[],
            "handler": "donaciones#getDonaciones"
        },
        "post":{
            "middleware":[],
            "handler": "donaciones#getDonaciones"
        }
    },

    "/api/centros": {
        "get":{
            "middleware":[],
            "handler": "centros#getCentros"
        },
        "post":{
            "middleware":[],
            "handler": "centros#postCentros"
        }
    },

    "/api/paises": {
        "get":{
            "middleware":[],
            "handler": "lugares#getPaises"
        }
    },

    "/api/estados": {
        "get":{
            "middleware":[],
            "handler": "lugares#getEstados"
        }
    },

    "/api/ciudades": {
        "get":{
            "middleware":[],
            "handler": "lugares#getCiudades"
        }
    },

    "/api/historias-medicas": {
        "get":{
            "middleware":[],
            "handler": "historias-medicas#getHistoriasMedicas"
        },
        "post":{
            "middleware":[],
            "handler": "historias-medicas#postHistoriasMedicas"
        }
    },

    "/api/lugares-recepcion": {
        "post":{
            "middleware":[],
            "handler": "lugares#postLugaresParaRecepcion"
        }
    },

    "/api/lugares-donacion": {
        "post":{
            "middleware":[],
            "handler": "lugares#postLugaresParaDonacion"
        }
    }
};