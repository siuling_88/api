var mysql      = require('mysql');
var connection = mysql.createConnection({
    // host     : 'localhost',
    // user     : 'retou',
    // password : 'retou',
    // database : 'RetoU'
    host     : '104.238.102.95',
    user     : 'siuling8_jchan',
    password : 'LasArdillas!!',
    database : 'siuling8_reto_solidaridad'
});


//Funcion que chequea las credenciales del usuario
function login(email, password, callback) {
    console.log("entro al login");

    this.getUsuarioByMail(email, function (err, results) {

        if (err) return callback(err);
        if (results.length === 0) return callback(null, {msg: 'Usuario o password Incorrecto'});
        var user = results[0];

        console.log("Esta dentro de getUsuarioByMail");

        if (password === user.contraseña) {
            callback(null, {
                id: user.id_usuario.toString(),
                id_tipo_usuario: user.id_tipo_usuario,
                nombre: user.nombre,
                // email: user.email,
                // identificacion: user.cedula_rif,
                // telefono: user.telefono
            });

        }else{
            callback(null, {msg: 'Usuario o password Incorrecto'});
        };
    });
}

//Funciones para obtener los usuarios del sistema segun su id
function getUsuarioById(id, callback){

    console.log('#######');
    console.log(id);

    var query = "SELECT * FROM usuario WHERE id_usuario = ?";

    connection.query(query, [id], function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');
    });
}

//Funciones para obtener los usuarios del sistema segun su correo
function getUsuarioByMail(email, callback){

    var query = "SELECT * FROM usuario WHERE email = ?";

    connection.query(query, [email], function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');
    });
}

//Funciones para obtener los usuarios del sistema
function getUsuarios(callback){

    connection.query('SELECT * from usuario', function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');
    });
}

//Funcion para agregar un usuario al sistema
function insertUsuarios(user, callback){

    var query = 'INSERT INTO usuario SET ?';

    connection.query(query, user, function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener los medicos del sistema
function getMedicos(callback){

    // var query ='SELECT * from medico AS M INNER JOIN usuario AS U ON M.id_usuario=U.id_usuario';
    var query ='SELECT * from medico';

    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones insertar medicos al sistema
function insertMedico(medico, callback){

    var query = 'INSERT INTO medico SET ?';

    connection.query(query, medico, function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener los centros del sistema
function getCentros(callback){


    // var query ='SELECT * from centro_distribucion as CD INNER JOIN usuario AS U ON CD.id_usuario=U.id_usuario ' +
    //     'INNER JOIN ciudad as C ON CD.id_ciudad=C.id_ciudad ';

    var query ='SELECT U.nombre as usuario_nombre, CD.imagen, CD.id_centro, CD.direccion, C.*,E.nombre as estado_nombre, P.nombre as ' +
        'pais_nombre from ciudad AS C INNER JOIN estado AS E ON C.id_estado=E.id INNER JOIN pais AS P ON ' +
        'E.id_pais=P.id_pais INNER JOIN centro_distribucion AS CD ON CD.id_ciudad = C.id_ciudad INNER JOIN usuario ' +
        'AS U ON CD.id_usuario=U.id_usuario';

    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones insertar Centros al sistema
function insertCentros(callback){

    console.log("Entro en insertHistoriasMedicas de BD.js")
    var query ='INSERT INTO centro_distribucion SET ?';

    var datos = {
        "id_centro":1,
        "direccion":2,
        "id_ciudad":3

    }

    connection.query(query, datos, function(err, rows, fields) {
        if (err) throw err;

        if (!err)     {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener las historias medicas del sistema
function getHistoriasMedicas(callback){

    var query ='' +
        'SELECT ' +
                    'HM.id as id_historiamedica, ' +
                    'HM.fecha_solicitud, ' +
                    'HM.nombre_doctor, ' +
                    'HM.mpps_doctor, ' +
                    'HM.observacion, ' +
                    'HM.id_paciente, ' +
                    'HM.id_solicitante, ' +
                    'MS.*, ' +
                    'P.concentracion, ' +
                    'P.id_medicamento, ' +
                    'P.inventario, ' +
                    'P.imagen, ' +
                    'P.unidad_medida, ' +
                    'P.id as id_presentacion, ' +
                    'M.nombre AS nombre_medicamento, ' +
                    'C.nombre as nombre_componente, ' +
                    'C.id as id_componente, ' +
                    'U.nombre as nombre_paciente ' +
        'FROM ' +
                    'historia_medica as HM INNER JOIN medicamentos_solicitados as MS ON ' +
                    'HM.id = MS.id_historia_medica INNER JOIN presentaciones as P ON MS.id_presentaciones = P.id INNER JOIN ' +
                    'medicamento AS M ON M.id = P.id_medicamento INNER JOIN componente AS C ON C.id = M.id_componente INNER JOIN ' +
                    'usuario AS U ON U.id_usuario = HM.id_solicitante';
// id_medicamento,inventario,imagen
    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        if (!err)     {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para insertar historias medicas al sistema
function insertHistoriasMedicas(medicalHistory,callback){
    console.log("Entro en insertHistoriasMedicas de BD.js")
    var query ='INSERT INTO historia_medica SET ?';
    //
    // var datos = {
    //     "id":1,
    //     "fecha_solicitud":1,
    //     "nombre_doctor":1,
    //     "mpps_doctor":1,
    //     "observacion":1,
    //     "id_paciente":1,
    //     "id_solicitante":1
    // }

    connection.query(query, medicalHistory, function(err, rows, fields) {
        if (err) throw err;

        if (!err)     {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para insertar lugares_para_donacion al sistema
function insertLugaresParaDonacion(datos,callback){
    console.log("Entro en insertLugaresParaDonacion de BD.js")
    var query ='INSERT INTO lugares_para_donacion SET ?';

    // var datos = {
    //     // "id_lugar_para_donacion":1,
    //     "id_donacion":2,
    //     "id_centro_distribucion":3
    // }

    connection.query(query, datos, function(err, rows, fields) {
        if (err) throw err;

        if (!err)     {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}


//Funciones para insertar lugares_para_donacion al sistema
function insertLugaresParaRecepcion(datos,callback){
    console.log("Entro en insertLugaresParaRecepcion de BD.js")
    var query ='INSERT INTO lugares_para_recepcion SET ?';

    // var datos = {
    //     "id_lugare_para_recepcion":1,
    //     "id_medicamento_solicitado":2,
    //     "id_centro_distribucion":3
    // }

    connection.query(query, datos, function(err, rows, fields) {
        if (err) throw err;

        if (!err)     {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}


//Funciones para obtener los medicamentos del sistema
function getMedicamentos(callback){

    var query ='SELECT P.id as id_presentacion, P.id_medicamento, P.inventario, P.concentracion, P.unidad_medida, P.imagen,  ' +
        'M.nombre as nombre_medicamento, ' +
        'C.id as id_componente, C.nombre as nombre_componente ' +
        'FROM presentaciones AS P INNER JOIN medicamento AS M ON P.id_medicamento=M.id INNER JOIN componente AS C ' +
        'ON M.id_componente=C.id';
    // var query ='SELECT * from medicamento';

    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para insertar medicamentos al sistema
function insertMedicamentos(callback){
    console.log("ENtro en insertMedicamentos de BD.js")
    var query ='INSERT INTO medicamento SET ?';

    var donacion = {
        "id":1,
        "nombre":2,
        "id_componente":3
    }

    connection.query(query, donacion, function(err, rows, fields) {
        if (err) throw err;

        if (!err)     {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para insertar status_donacion al sistema
function insertStatusDonacion(callback){
    console.log("ENtro en insertStatusDonacion de BD.js")
    var query ='INSERT INTO status_donacion SET ?';

    var donacion = {
        "id_status":1,
        "status": "Entregadouuu",
    }

    connection.query(query, donacion, function(err, rows, fields) {
        if (err) throw err;

        if (!err)     {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener los paises del sistema
function getPaises(callback){

    var query ='SELECT * from pais';

    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener los estados del sistema
function getEstados(callback){

    var query ='SELECT E.*, P.id_pais, P.nombre as nombre_pais from estado AS E INNER JOIN pais AS P ON E.id_pais=P.id_pais';

    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        if (!err){
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener las ciudades del sistema
function getCiudades(callback){

    var query ='SELECT C.*,E.nombre as estado_nombre, P.nombre as pais_nombre from ciudad AS C INNER JOIN' +
        ' estado AS E ON C.id_estado=E.id INNER JOIN pais AS P ON E.id_pais=P.id_pais';

    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funcion para obtener toda la informacion de una donacion, dado un idUsuario
function getDonaciones(idUsuario, callback){
    console.log("Entro a get Donaciones en el archivo bd.js")
    var query ='' +
        'SELECT ' +
                'D.*,' +
                'CD.direccion, CD.imagen,' +
                'U.nombre as nombre_centro,' +
                'ciudad.ciudad_nombre,' +
                'estado.nombre AS estado_nombre,' +
                'pais.nombre AS pais_nombre,' +
                'S.status,' +

                'MS.id_historia_medica,' +
                'U2.nombre AS nombre_solicitante,' +
                'P.concentracion,' +
                'P.unidad_medida,' +
                'P.imagen,' +
                'P.inventario,' +
                'M.nombre AS nombre_medicamento,' +
                'C.nombre AS nombre_componente,' +
                'U2.nombre AS nombre_paciente' +

        ' FROM ' +

                'donacion AS D,' +
                'lugares_para_donacion AS LPD,' +
                'centro_distribucion AS CD,' +
                'usuario AS U,' +
                'ciudad,' +
                'estado,' +
                'pais,' +
                'status_donacion AS S,' +

                'usuario AS U2,' +
                'medicamentos_solicitados AS MS,' +
                'presentaciones AS P,' +
                'medicamento AS M,' +
                'componente AS C,' +
                'historia_medica AS HM' +

        ' WHERE ' +

                'D.id_usuario_donador = ? ' +
                'AND	LPD.id_donacion = D.id_donacion ' +
                'AND	LPD.id_centro_distribucion = CD.id_centro ' +
                'AND	CD.id_usuario = U.id_usuario ' +
                'AND	CD.id_ciudad = ciudad.id_ciudad ' +
                'AND	ciudad.id_estado = estado.id ' +
                'AND	estado.id_pais = pais.id_pais ' +
                'AND	S.id_status = D.id_status ' +

                'AND	MS.id_historia_medica = HM.id ' +
                'AND	MS.id_medicamento_solicitado = D.id_medicamento_solicitado ' +
                'AND	MS.id_presentaciones = P.id ' +
                'AND	P.id_medicamento = M.id ' +
                'AND	M.id_componente = C.id ' +
                'AND	U2.id_usuario = HM.id_solicitante;'

    connection.query(query, idUsuario ,function(err, rows, fields) {
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}


function insertDonacion(donacion, callback){
    console.log("ENtro en insertDonacion de BD.js")
    var query ='INSERT INTO donacion SET ?';


    connection.query(query, donacion, function(err, rows, fields) {
        if (err) throw err;

        if (!err)	  	{
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

function getSolicitudes(user, callback){
    console.log("ENtro en getSolicitudes de BD.js y el usuario es:");
    console.log(user);
    var query ='SELECT HM.id as id_historia_medica, HM.id_solicitante, ' +
        ' MS.cantidad_en_espera, MS.cantidad_por_entregar, MS.cantidad_solicitada, ' +
        ' MS.cantidad_recibida, MS.id_medicamento_solicitado, ' +
        ' P.concentracion, P.unidad_medida, P.imagen, ' +
        ' M.nombre as nombre_medicamento, ' +
        ' C.nombre as nombre_componente, ' +
        "  FROM_UNIXTIME(unix_timestamp(D.fecha_entrega_centro_distribucion), '%d-%m-%Y') as fecha_entrega_centro_distribucion, D.cantidad_donada, D.id_donacion, " +
        ' CD.direccion, CD.imagen as imagen_centro, CD.id_ciudad, ' +
        ' ciudad.ciudad_nombre, U2.nombre AS nombre_centro ' +
        ' FROM ' +
        ' historia_medica AS HM INNER JOIN medicamentos_solicitados AS MS ON HM.id = MS.id_historia_medica ' +
        ' INNER JOIN presentaciones AS P ON MS.id_presentaciones = P.id ' +
        ' INNER JOIN medicamento AS M ON P.id_medicamento = M.id ' +
        ' INNER JOIN componente AS C ON M.id_componente = C.id ' +
        ' LEFT JOIN donacion AS D ON MS.id_medicamento_solicitado = D.id_medicamento_solicitado ' +
        ' LEFT JOIN centro_distribucion AS CD ON CD.id_centro = D.id_centro_distribucion_receptor ' +
        ' LEFT JOIN ciudad ON ciudad.id_ciudad = CD.id_ciudad ' +
        ' LEFT JOIN usuario AS U2 ON U2.id_usuario = CD.id_usuario ' +
        ' WHERE ' +
        // ' D.fecha_entrega_centro_distribucion is not NULL ' +
        ' D.fecha_entrega_receptor is NULL' +
        ' AND HM.id_solicitante = ?' +
        ' ORDER BY id_historia_medica, id_medicamento_solicitado';

    connection.query(query, user.userid, function(err, rows, fields) {
        if (err) throw err;

        if (!err) {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

function insertSolicitud(solicitud, callback){
    console.log("ENtro en insertSolicitud de BD.js y lo q se va a insertar es:");
    console.log(solicitud)
    var query ='INSERT INTO medicamentos_solicitados SET ?';


    connection.query(query, solicitud, function(err, rows, fields) {
        if (err) throw err;

        if (!err) {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener los donaciones realizadas por el usuario.
function getDonadores(centroid, callback){

    console.log("Entro en getDonadores de bd.js")
    // console.log(userId);
    console.log(centroid);
    var query = 'SELECT D.cantidad_donada, D.id_donacion, U.nombre as nombre_donador, U.cedula_rif, MS.*, ' +
        ' P.concentracion, P.unidad_medida, P.imagen, M.nombre as nombre_medicamento, C.nombre as nombre_componente ' +
        ' FROM ' +
        ' donacion AS D, usuario AS U, lugares_para_donacion AS LPD, centro_distribucion AS CD, ' +
        ' medicamentos_solicitados AS MS, presentaciones AS P, medicamento AS M, componente AS C ' +
        ' WHERE ' +
        ' CD.id_usuario = ? AND D.fecha_entrega_centro_distribucion IS NULL AND D.id_centro_distribucion_entrega = 0  ' +
        ' AND LPD.id_centro_distribucion = CD.id_centro AND LPD.id_donacion = D.id_donacion ' +
        ' AND D.id_usuario_donador = U.id_usuario AND D.id_medicamento_solicitado = MS.id_medicamento_solicitado ' +
        ' AND MS.id_presentaciones = P.id AND M.id = P.id_medicamento AND M.id_componente = C.id' +
        ' ORDER BY' +
        ' U.cedula_rif, U.nombre';
    console.log("El query en getDonadores es:");
    console.log(query);
    console.log("El centroid antes del query vale: " + centroid);

    connection.query(query, centroid, function(err, rows, fields) {
        console.log("Entro en query getDonadores2 de bd.js")
        console.log("El centroid vale: " + centroid);
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

function getReceptores(centro, callback){
    console.log("ENtro en getReceptores de BD.js y el centro es:");
    console.log(centro);
    var query ='SELECT HM.id as id_historia_medica, HM.id_solicitante, ' +
        ' MS.cantidad_en_espera, MS.cantidad_por_entregar, MS.cantidad_solicitada, ' +
        ' MS.cantidad_recibida, MS.id_medicamento_solicitado, ' +
        ' P.concentracion, P.unidad_medida, P.imagen, ' +
        ' M.nombre as nombre_medicamento, ' +
        ' C.nombre as nombre_componente, ' +
        "  FROM_UNIXTIME(unix_timestamp(D.fecha_entrega_centro_distribucion), '%d-%m-%Y') as fecha_entrega_centro_distribucion, D.cantidad_donada, D.id_donacion, " +
        ' CD.direccion, CD.imagen as imagen_centro, CD.id_ciudad, ' +
        ' ciudad.ciudad_nombre, U2.nombre AS nombre_centro, U.nombre as nombre_receptor, U.cedula_rif as cedula_receptor ' +
        ' FROM ' +
        ' historia_medica AS HM INNER JOIN medicamentos_solicitados AS MS ON HM.id = MS.id_historia_medica ' +
        ' INNER JOIN presentaciones AS P ON MS.id_presentaciones = P.id ' +
        ' INNER JOIN medicamento AS M ON P.id_medicamento = M.id ' +
        ' INNER JOIN componente AS C ON M.id_componente = C.id ' +
        ' LEFT JOIN donacion AS D ON MS.id_medicamento_solicitado = D.id_medicamento_solicitado ' +
        ' LEFT JOIN centro_distribucion AS CD ON CD.id_centro = D.id_centro_distribucion_receptor ' +
        ' LEFT JOIN ciudad ON ciudad.id_ciudad = CD.id_ciudad ' +
        ' LEFT JOIN usuario AS U2 ON U2.id_usuario = CD.id_usuario ' +
        ' LEFT JOIN usuario AS U ON U.id_usuario = HM.id_paciente ' +
        ' WHERE ' +

        ' D.fecha_entrega_receptor is NULL ' +
        ' AND U2.id_usuario = ?' +
        ' AND D.id_centro_distribucion_receptor = CD.id_centro ' +

        ' ORDER BY id_historia_medica, id_medicamento_solicitado'

    connection.query(query, centro.centroid, function(err, rows, fields) {
        if (err) throw err;

        if (!err) {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

//Funciones para obtener los donaciones realizadas por el usuario.
function getDonacionesByUserId(userId, callback){

    console.log("Entro en getDonacionesByUserId de bd.js")
    console.log(userId);
//
//     cantidad_recibida
//     Ya fue entregado al paciente
//     cantidad_en_espera
//     Aun no se entrega al centro de distribucion
//     cantidad_por_entregar
//     Recibido por el centro de distribucion pero no ha …
// cantidad_solicitada

    var query = 'SELECT D.*, CD.direccion, CD.imagen, U.nombre as nombre_centro, ciudad.ciudad_nombre, ' +
        'estado.nombre AS estado_nombre, pais.nombre AS pais_nombre, S.status, ' +
        'MS.id_historia_medica, MS.cantidad_en_espera, MS.cantidad_por_entregar, MS.cantidad_solicitada, MS.cantidad_recibida, ' +
        'U2.nombre AS nombre_solicitante, P.concentracion, P.unidad_medida, P.imagen, P.inventario, ' +
        'M.nombre AS nombre_medicamento, C.nombre AS nombre_componente,U2.nombre AS nombre_paciente  ' +
        'FROM  ' +
        'donacion AS D, lugares_para_donacion AS LPD, centro_distribucion AS CD, usuario AS U, ciudad, estado, ' +
        'pais, status_donacion AS S, usuario AS U2, medicamentos_solicitados AS MS, presentaciones AS P, ' +
        'medicamento AS M, componente AS C, historia_medica AS HM  WHERE D.id_usuario_donador = \''+ userId.userId +'\' ' +
        'AND LPD.id_donacion = D.id_donacion AND LPD.id_centro_distribucion = CD.id_centro AND CD.id_usuario = U.id_usuario ' +
        'AND CD.id_ciudad = ciudad.id_ciudad AND ciudad.id_estado = estado.id AND estado.id_pais = pais.id_pais ' +
        'AND S.id_status = D.id_status AND MS.id_historia_medica = HM.id AND MS.id_medicamento_solicitado = D.id_medicamento_solicitado ' +
        'AND MS.id_presentaciones = P.id AND P.id_medicamento = M.id AND M.id_componente = C.id AND U2.id_usuario = HM.id_solicitante ' +
        'ORDER BY id_historia_medica, id_donacion';
    console.log(query);
    connection.query(query, userId, function(err, rows, fields) {
        console.log("Entro en query getDonacionesByUserId de bd.js")
        if (err) throw err;

        if (!err)       {
            console.log(rows);
            return callback(null, rows);
        }
        else
            return callback('Error while performing Query.');

    });
}

module.exports.login = login;

module.exports.getUsuarioById = getUsuarioById;
module.exports.getUsuarioByMail = getUsuarioByMail;

module.exports.getUsuarios = getUsuarios;
module.exports.insertUsuarios = insertUsuarios;

module.exports.getMedicos = getMedicos;
module.exports.insertMedico = insertMedico;

module.exports.getCentros = getCentros;
module.exports.insertCentros = insertCentros;

module.exports.getHistoriasMedicas = getHistoriasMedicas;
module.exports.insertHistoriasMedicas = insertHistoriasMedicas;

module.exports.insertLugaresParaDonacion = insertLugaresParaDonacion;

module.exports.insertLugaresParaRecepcion = insertLugaresParaRecepcion;

module.exports.getMedicamentos = getMedicamentos;
module.exports.insertMedicamentos = insertMedicamentos;

module.exports.insertStatusDonacion = insertStatusDonacion;

module.exports.insertSolicitud = insertSolicitud;

module.exports.insertDonacion = insertDonacion;
module.exports.getDonacionesByUserId = getDonacionesByUserId;
module.exports.getDonadores = getDonadores;
module.exports.getDonaciones = getDonaciones;

module.exports.getSolicitudes = getSolicitudes;
module.exports.getReceptores = getReceptores;

module.exports.getPaises = getPaises;
module.exports.getEstados = getEstados;
module.exports.getCiudades = getCiudades;


