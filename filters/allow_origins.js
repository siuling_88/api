module.exports = function(req, res, next) {
  	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'POST, DELETE, GET, PUT, PATCH, OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, token');
	res.header('Access-Control-Expose-Headers', 'Origin, X-Requested-With, Content-Type, Accept, token');
	res.header('Access-Control-Allow-Credentials', 'true');

	if ('OPTIONS' == req.method) {
    	res.sendStatus(200).end();
    
    } else {
	  	next();
	  	
	}
};


